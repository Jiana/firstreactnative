/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
//     "start": "node node_modules/react-native/local-cli/cli.js start",

// const React = require('react-native-web')
import React, {
  AppRegistry,
  AppState,
  Component,
  Navigator,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Switch,
  Text,
  View,
  Alert,
  TouchableHighlight,
  NativeModules,
  PushNotificationIOS
} from 'react-native';

import CodePush from 'react-native-code-push';
import styles from './style';
var RCTNativeAppEventEmitter = require('RCTNativeAppEventEmitter');

var MiddleAds = require('./components/MiddleAds');
var Search = require('./components/Search');
// var HtlListView = require('./components/HtlListView');
import HtlListView from './components/HtlListView';

var reactViewWidth = 101;
var reactViewHeight = 102;
var newReactViewWidth = 201;
var newReactViewHeight = 202;

class FirstProject extends Component {
  constructor(props) {
    super(props)
    // this.state = {
    //   hotels: [
    //     {"hotelId":"HTLI000000310","hotelName":"台北晶華酒店","hotelOrderStatus":"NORMAL","photoURL":"http://www.eztravel.com.tw/ss_static/images/htl/more2/0/HTLI000000310_m017.jpg","salePriceAvg":"5940"},
    //     {"hotelId":"HTLI000004178","hotelName":"台北睡覺盒子旅店-西門館","hotelOrderStatus":"NORMAL","photoURL":"http://www.eztravel.com.tw/ss_static/images/htl/more2/8/HTLI000004178_m01.jpg","salePriceAvg":"739"},
    //     {"hotelId":"HTLI000003423","hotelName":"台北太空艙旅舍【全新開幕】","hotelOrderStatus":"NORMAL","photoURL":"http://www.eztravel.com.tw/ss_static/images/htl/more/HTLI000003423_m01.jpg","salePriceAvg":"640"},
    //     {"hotelId":"HTLI000003369","hotelName":"台北梅樓商務驛站","hotelOrderStatus":"NORMAL","photoURL":"http://www.eztravel.com.tw/ss_static/images/htl/more/HTLI000003369_m01.jpg","salePriceAvg":"675"}]
    // };
  }
  // componentDidMount() {
  //   console.log("Initial CodePush Log=======================");
  //   // CodePush.sync({
  //   //   updateDialog: false,
  //   //   installMode: CodePush.InstallMode.IMMEDIATE,
  //   //   deploymentKey: "zko7tLexWtsdCiW6h_8sLCobSARXNyeRmEuJ-"
  //   // });
  // }

  componentDidMount() {
    // this.ref._containView.measure(this._containHeight);
  }
  componentWillMount() {
    // CodePush.sync({
    //   updateDialog: false,
    //   installMode: CodePush.InstallMode.IMMEDIATE,
    //   deploymentKey: "zko7tLexWtsdCiW6h_8sLCobSARXNyeRmEuJ-"
    // });
  }

  _containHeightMeasure(ox, oy, width, height, px, py) {
    console.log("ox: " + ox);
    console.log("oy: " + oy);
    console.log("width: " + width);
    console.log("height: " + height);
    console.log("px: " + px);
    console.log("py: " + py);
  }

  _scrollHeight() {
    var adLen = Math.ceil(this.props.hotels.length / 2) * 120;
    return ({
      height: adLen,
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: "flex-start",
      paddingLeft: 5,
      paddingRight: 5,
      backgroundColor: "#FFFFFF"
    })
  }
  render() {
    PushNotificationIOS.requestPermissions();
    PushNotificationIOS.setApplicationIconBadgeNumber(1);
    var adNodes = this.props.hotels.map(function(ads) {
      return (
        <View key={ads.hotelId} style={styles.middleAdItems}>
          <MiddleAds htl={ads}/>
        </View>
      );
    });
    return (
      <View ref="_containView">
        <View style={styles.hotTitle}>
          <Text style={styles.titleTxt}>
            熱門推薦
          </Text>
        </View>
        <View style={this._scrollHeight()}>
           {adNodes}
        </View>

      </View>
    );
  }
}

//
// <View style={this._scrollHeight()}>
//     {adNodes}
// </View>
// <ScrollView
//   ref={(scrollView) => { _scrollView = scrollView; }}
//   onLayout={(e)=>{
//     this.
//   }}
//   contentContainerStyle={this._scrollHeight()}>
//   {adNodes}
// </ScrollView>
//
// class NavProject extends Component {
//
//   constructor(props) {
//     super(props)
//   }
//   componentDidMount() {
//     console.log("Initial CodePush Log=======================");
//     // CodePush.sync({
//     //   updateDialog: true,
//     //   installMode: CodePush.InstallMode.IMMEDIATE,
//     //   deploymentKey: "zko7tLexWtsdCiW6h_8sLCobSARXNyeRmEuJ-"
//     // });
//   }
//   render() {
//     return (
//       <Navigator
//         sceneStyle={styles.container}
//         configureScene={(route, routeStack) => Navigator.SceneConfigs.FloatFromRight}
//         initialRouteStack={[{name: 'My First Scene', index: 0}]}
//         renderScene={(route, navigator) => {
//           var sceneNode = () => {
//             if(route.name == "SearchExample"){
//               return(
//                 <View style={styles.downV}>
//                   <SearchExample hotels={this.props.hotels}/>
//                 </View>
//
//               )
//             }else if(route.name == "ListExample") {
//               return(
//                 <View style={styles.downV}>
//                   <ListExample hotels={this.props.hotels}/>
//                 </View>
//
//               )
//             }else {
//               return(
//                 <View style={styles.downV}>
//                 <ScrollView
//                   contentContainerStyle={styles.downV}>
//                   <TouchableHighlight onPress={() => {
//                     navigator.push({
//                       name: "SearchExample",
//                       index: 1,
//                     });
//                   }}>
//                     <Text style={styles.navContentTitle}>
//                       {"SearchExample"}
//                     </Text>
//                   </TouchableHighlight>
//                   <TouchableHighlight onPress={() => {
//                     navigator.push({
//                       name: "ListExample",
//                       index: 1,
//                     });
//                   }}>
//                     <Text style={styles.navContentTitle}>
//                       {"ListExample"}
//                     </Text>
//                   </TouchableHighlight>
//                 </ScrollView>
//                 </View>
//               )
//             }
//           }
//           return (
//             <View style={styles.topV}>
//               <NavExample
//                 senceProp={route}
//                 navigator={navigator}
//                 onForward={(txtNm) => {
//                   navigator.push({
//                     name: txtNm,
//                     index: 1,
//                   });
//                 }}
//                 onBack={() => {
//                   if (route.index > 0) {
//                     navigator.pop();
//                   }
//                 }}
//               />
//             {sceneNode()}
//             </View>
//           )
//         }}
//       />
//
//     );
//   }
// }
//
// const SearchExample = React.createClass({
//   getInitialState() {
//     return {
//       dataOnView: this.props.hotels,
//       searchBar: false
//     }
//   },
//   _scrollHeight: function(){
//     var adLen = Math.ceil(this.state.dataOnView.length / 2) * 120;
//     return ({
//       height: adLen,
//       flexDirection: 'row',
//       flexWrap: 'wrap',
//       justifyContent: "flex-start",
//       paddingLeft: 5,
//       paddingRight: 5,
//     })
//   },
//   _changeHandler: function(data){
//     if(!data){
//       this.state.dataOnView = this.props.hotels;
//     }else {
//       this.state.dataOnView = data;
//     }
//     this.forceUpdate()
//   },
//   render: function() {
//     var _scrollView: ScrollView;
//     var adNodes = this.state.dataOnView.map(function(ads) {
//       return (
//         <View key={ads.hotelId} style={styles.middleAdItems}>
//           <MiddleAds htl={ads}/>
//         </View>
//       );
//     });
//     var searchBarView = () => {
//       return (
//         <Search data={this.props.hotels} onChange={this._changeHandler}/>
//       );
//     }
//     return (
//       <View>
//         <View style={styles.hotTitle}>
//           <Text style={styles.titleTxt}>
//             熱門推薦
//           </Text>
//           <Switch
//             onValueChange={(value) => {
//               this.state.searchBar = value;
//               this.forceUpdate()
//               if(!value) this._changeHandler()
//             }}
//             onTintColor={"#81C31E"}
//             style={styles.searchSwitch}
//             value={this.state.searchBar} />
//         </View>
//         {this.state.searchBar && searchBarView()}
//         <View>
//           <ScrollView
//             ref={(scrollView) => { _scrollView = scrollView; }}
//             onLayout={(e)=>{}}
//             contentContainerStyle={this._scrollHeight()}>
//             {adNodes}
//           </ScrollView>
//         </View>
//       </View>
//     );
//   }
//
// });
//
// const ListExample = React.createClass({
//
//   render: function() {
//     var listContentView = () => {
//       console.log("listContentView");
//       return (
//         <HtlListView htl={this.props.hotels}/>
//       );
//     }
//     return (
//       <View>
//         {listContentView()}
//       </View>
//     );
//   }
//
// });
//
// const NavExample = React.createClass({
//
//   getInitialState() {
//     return {
//       scenes: ["SearchExample", "ListExample"]
//     }
//   },
//   _onPrev: function() {
//     console.log("calling next...");
//     this.props.onBack();
//   },
//   _onNext: function() {
//     console.log("calling next...");
//     this.props.onForward();
//   },
//   // _onPressButton: function() {
//   //   console.log("hi");
//   // },
//   render: function() {
//     console.log(this.props.senceProp.name);
//     var sceneNode = this.state.scenes.map((s)=>{
//       var _onPressButton = () => {
//         this.props.onForward(s);
//       };
//       return (
//         <View key={s}>
//           <TouchableHighlight onPress={_onPressButton}>
//             <Text style={styles.navContentTitle}>
//               {s}
//             </Text>
//           </TouchableHighlight>
//         </View>
//       );
//     });
//     var scenesTop = () => {
//       if(this.props.senceProp.name !== "My First Scene"){
//         return (
//           <TouchableHighlight onPress={this._onPrev}>
//             <Text style={styles.titleTxt}>
//               {"Back"}
//             </Text>
//           </TouchableHighlight>
//         )
//       }
//     }
//     return (
//       <View>
//         <View style={styles.hotTitle}>
//           <Text style={styles.titleTxt}>
//             {this.props.senceProp.name}
//           </Text>
//           {scenesTop()}
//         </View>
//       </View>
//     );
//   }
//
// });

AppRegistry.registerComponent('FirstProject', () => FirstProject);



// {<TouchableOpacity
//   style={styles.touchOpa}
// onPress={() => { _scrollView.scrollTo({y: 0}); }}>
//   <Text>Scroll to top</Text>
// </TouchableOpacity>}


// fetch("http://10.10.114.192:5000/api/1/rn_update/ios", {method: "GET"})
//         .then((response) => response.json())
//         .then((responseData) => {
//           if(responseData.update){
//             Alert.alert(
//               'An Update Available',
//               'Would you like to install it?',
//               [
//                 {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
//                 {text: 'Get now!', onPress: () => console.log('Get now! Pressed')},
//               ]
//             );
//           }
//         })
//         .done();
