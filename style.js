import React, {
  StyleSheet,
  PixelRatio
} from 'react-native';
import Phone from "./dimensions.js";
import Colors from "./color.js";

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
    width: Phone.DEVICE_WIDTH,
    height: Phone.DEVICE_HEIGHT,
    backgroundColor: Colors.color0,
  },
  topV: {
    height: 70,
  },
  downV: {
    height: Phone.DEVICE_HEIGHT - 70,
  },
  navContentTitle: {
    height: 70,
  },
  hotTitle: {
    marginTop: 8,
    marginBottom: 8,
    borderLeftWidth: 5,
    borderColor: Colors.ciOrange,
    // borderColor: Colors.ciGreen,
    flexWrap: "nowrap",
    flexDirection: 'row',
  },
  titleTxt: {
    fontWeight: "bold",
    fontSize: 16,
    paddingLeft: 6,
    flex: 4
  },
  searchSwitch: {
    height: 14*PixelRatio.get(),
    flex: 1,
  },
  searchBar: {
    padding: 10,
  },
  searchInput: {
    height: 30,
    fontSize: 12,
    padding: 5,
    paddingHorizontal: 8,
    borderRadius: 3,
    borderWidth: 1,
  },
  touchOpa: {
    margin: 7,
    padding: 5,
    alignItems: 'center',
    backgroundColor: 'rgba(51,51,51,0.1)',
    borderRadius: 100,
  },
  gridView: {
    flex: 1,
  },
  middleAdSection: {
    // height: 100,
    // flexDirection: 'row',
    // flexWrap: 'wrap',
    // justifyContent: "",
    // paddingLeft: 5,
    // paddingRight: 5,
  },
  middleAdItems: {
    width: (Phone.DEVICE_WIDTH - 10)/2,
    height: 120,
    padding: 5,
  },
  floatTxt: {
    padding: 5,
    backgroundColor: Colors.color7,
  },
  normalTxt: {
  	textAlign: "left",
    flexWrap: 'nowrap',
    height: 12,
    fontSize: 10,
    overflow: 'hidden',
  },
  priceTxt: {
  	textAlign: "right",
    fontSize: 10,
    paddingTop: 5
  },
  price: {
  	color: Colors.ciOrange,
    fontSize: 10,
    fontWeight: "bold",
  },
  listViewContent: {
    flexDirection: 'row',
    height: 70,
  },
  separator: {
    height: 2,
    backgroundColor: Colors.color7,
  },
  textAreaList: {
    padding: 8,
    flex: 8,
  },
  imgViewList: {
    backgroundColor: Colors.color6,
    // width: (Phone.DEVICE_WIDTH - 30)/2,
    // height: 70,
    flex: 9,
  },
  imgView: {
    height: 70,
  },
  imgViewDe4: {
    width: (Phone.DEVICE_WIDTH - 30)/2,
    height: 70,
    backgroundColor: Colors.color6,
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 40
  }
});

module.exports = styles;
