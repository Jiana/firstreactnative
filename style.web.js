import React, {
  StyleSheet
} from 'react-native-web';
// import Colors from "./color.js";
var Colors = {
  color0: '#000000',
  color1: '#333333',
  color11: '#383F45',
  color2: '#666666',
  color3: '#999999',
  color4: '#C1C1C1',
  color5: '#CCCCCC',
  color6: '#DFDFDF',
  color7: '#F4F4F4',
  white0: '#FFFFFF',
  color44: '#CAD1D9',
  ezGreen: '#00AA00',
  canGreen: '#2EAB00',
  liGreen: '#6FBE00',
  ciGreen: '#81C31E',
  calGreen: '#9ECD58',
  canRed: '#F12600',
  ciOrange: '#FF7200',
  calOrange: '#FF9646',
  liOrange: '#FCA920',
};
var Phone = {
  DEVICE_WIDTH: 400,
  DEVICE_HEIGHT: 700
};

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
    width: Phone.DEVICE_WIDTH,
    height: Phone.DEVICE_HEIGHT,
    backgroundColor: Colors.color0,
  },
  button: {
    backgroundColor: Colors.color0,
  },
  topV: {
    height: 70,
  },
  downV: {
    height: Phone.DEVICE_HEIGHT - 70,
  },
  navContentTitle: {
    height: 70,
  },
  hotTitle: {
    marginTop: 8,
    marginBottom: 8,
    borderLeftWidth: 5,
    borderColor: Colors.ciOrange,
    // borderColor: Colors.ciGreen,
    flexWrap: "nowrap",
    flexDirection: 'row',
  },
  titleTxt: {
    fontWeight: "bold",
    fontSize: 16,
    paddingLeft: 6,
    flex: 4
  },
  // searchSwitch: {
  //   height: 14*PixelRatio.get(),
  //   flex: 1,
  // },
  searchBar: {
    padding: 10,
  },
  searchInput: {
    height: 30,
    fontSize: 12,
    padding: 5,
    paddingHorizontal: 8,
    borderRadius: 3,
    borderWidth: 1,
  },
  touchOpa: {
    margin: 7,
    padding: 5,
    alignItems: 'center',
    backgroundColor: 'rgba(51,51,51,0.1)',
    borderRadius: 100,
  },
  gridView: {
    flex: 1,
  },
  middleAdSection: {
    // height: 100,
    // flexDirection: 'row',
    // flexWrap: 'wrap',
    // justifyContent: "",
    // paddingLeft: 5,
    // paddingRight: 5,
  },
  middleAdItems: {
    width: (Phone.DEVICE_WIDTH - 10)/2,
    height: 120,
    padding: 5,
  },
  floatTxt: {
    padding: 5,
    backgroundColor: Colors.color7,
  },
  normalTxt: {
  	textAlign: "left",
    flexWrap: 'nowrap',
    height: 12,
    fontSize: 10,
    overflow: 'hidden',
  },
  priceTxt: {
  	textAlign: "right",
    fontSize: 10,
    paddingTop: 5
  },
  price: {
  	color: Colors.ciOrange,
    fontSize: 10,
    fontWeight: "bold",
  },
  listViewContent: {
    flexDirection: 'row',
    height: 70,
  },
  separator: {
    height: 2,
    backgroundColor: Colors.color7,
  },
  textAreaList: {
    padding: 8,
    flex: 8,
  },
  imgViewList: {
    backgroundColor: Colors.color6,
    // width: (Phone.DEVICE_WIDTH - 30)/2,
    // height: 70,
    flex: 9,
  },
  imgView: {
    height: 70,
  },
  imgViewDe4: {
    width: (Phone.DEVICE_WIDTH - 30)/2,
    height: 70,
    backgroundColor: Colors.color6,
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 40
  }
});

module.exports = styles;
