/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
//     "start": "node node_modules/react-native/local-cli/cli.js start",

// const React = require('react-native-web')
import React, {
  AppRegistry,
  AppState,
  Component,
  ScrollView,
  Text,
  View,
  TouchableHighlight,
  StyleSheet
} from 'react-native-web';

import styles from './style.web';

var MiddleAds = require('./components/MiddleAds.web');

class FirstProject extends Component {
  constructor(props) {
    super(props)
    this.state = {
      hotels: [
        {"hotelId":"HTLI000000310","hotelName":"台北晶華酒店","hotelOrderStatus":"NORMAL","photoURL":"http://www.eztravel.com.tw/ss_static/images/htl/more2/0/HTLI000000310_m017.jpg","salePriceAvg":"5940"},
        {"hotelId":"HTLI000004178","hotelName":"台北睡覺盒子旅店-西門館","hotelOrderStatus":"NORMAL","photoURL":"http://www.eztravel.com.tw/ss_static/images/htl/more2/8/HTLI000004178_m01.jpg","salePriceAvg":"739"},
        {"hotelId":"HTLI000003423","hotelName":"台北太空艙旅舍【全新開幕】","hotelOrderStatus":"NORMAL","photoURL":"http://www.eztravel.com.tw/ss_static/images/htl/more/HTLI000003423_m01.jpg","salePriceAvg":"640"},
        {"hotelId":"HTLI000003369","hotelName":"台北梅樓商務驛站","hotelOrderStatus":"NORMAL","photoURL":"http://www.eztravel.com.tw/ss_static/images/htl/more/HTLI000003369_m01.jpg","salePriceAvg":"675"}]
    };
  }
  _scrollHeight() {
    var adLen = Math.ceil(this.state.hotels.length / 2) * 120;
    return ({
      height: adLen,
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: "flex-start",
      paddingLeft: 5,
      paddingRight: 5
    })
  }
  render() {
    var _scrollView: ScrollView;
    var adNodes = this.state.hotels.map(function(ads) {
      return (
        <View key={ads.hotelId} style={styles.middleAdItems}>
          <MiddleAds htl={ads}/>
        </View>
      );
    });
    return (
      <View>
        <View style={styles.hotTitle}>
          <Text style={styles.titleTxt}>
            熱門推薦
          </Text>
        </View>
        <View>
          <ScrollView
            ref={(scrollView) => { _scrollView = scrollView; }}
            onLayout={(e)=>{}}
            contentContainerStyle={this._scrollHeight()}>
            {adNodes}
          </ScrollView>
        </View>
      </View>
    );
  }
}

AppRegistry.registerComponent('FirstProject', () => FirstProject);
AppRegistry.runApplication('FirstProject', { rootTag: document.getElementById('react-root') })
