var config = {
   entry: './index.ios.web.js',

   output: {
      path:'./',
      filename: 'index.js',
   },

  //  devServer: {
  //     inline: true,
  //     port: 5566
  //  },
  resolve: {
    alias: {
      'react-native': 'react-native-web'
    }
  },

   module: {
      loaders: [ {
         test: /\.jsx?$/,
         exclude: /node_modules/,
         loader: 'babel',

         query: {
            presets: ['es2015', 'react']
         }
      }]
   }

}

module.exports = config;
