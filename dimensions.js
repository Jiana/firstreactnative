import Dimensions from 'Dimensions';

const x = Dimensions.get('window').width;
const y = Dimensions.get('window').height;

export default Phone = {
  DEVICE_WIDTH: x,
  DEVICE_HEIGHT: y,
}
