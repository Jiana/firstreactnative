'use strict';
// es6 practice

import React, {
  Component,
  Text,
  View,
  Image,
  ListView,
  TouchableHighlight
} from 'react-native';

import styles from '../style';

export default class HtlListView extends Component{
  constructor(props) {
    super(props)
    console.log("htl list view");
    console.log(props.htl);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => false});
    this.state = {
      dataSource: ds.cloneWithRows(props.htl)
    };
  }
  render() {
    return (
      <ListView
        dataSource={this.state.dataSource}
        renderRow={this._renderRow}
        renderSeparator={(sectionID, rowID) => <View key={`${sectionID}-${rowID}`} style={styles.separator} />}
      />

    );
  }
  _renderRow = (rowData, sectionID, rowID) => {
    var _onPressButton = () => {
      console.log("123");
      console.log(rowID);
    };
    return (
      <TouchableHighlight onPress={_onPressButton}>
        <View style={styles.listViewContent}>
          <View style={styles.imgViewList}>
            <Image
              style={styles.imgView}
              resizeMode={"cover"}
              onError={(e) => {
                console.log("failed img");
              }}
              source={{uri: rowData.photoURL}}/>
          </View>

          <View style={styles.textAreaList}>
            <Text style={styles.normalTxt}>
              {rowData.hotelName}
              {/*'\n'*/}
            </Text>
            <Text style={styles.priceTxt}>
              <Text style={styles.price}>
                {rowData.salePriceAvg}
              </Text> 起
            </Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}
