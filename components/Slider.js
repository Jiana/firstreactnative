'use strict';

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  requireNativeComponent,
  NativeModules
} from 'react-native';

var Slider = React.createClass({
  _onPressButton() {
    NativeModules.AppDelegate.processString("Goodbye", (text) => {
      this.setState({text});
      console.log(text);
    });
  },
  _onSlide() {
    var sliderThis = this;
    setInterval(function() {
      var currentItemIndex = sliderThis.state.currentItemIndex + 1;
      if(currentItemIndex >= sliderThis.props.data.length) currentItemIndex = 0;
      var currentItem = sliderThis.props.data[currentItemIndex];
      sliderThis.setState({
        currentItemIndex: currentItemIndex,
        currentItem: currentItem
      });
    }, 5000);
  },
  getInitialState() {
    return {
      currentItemIndex: 0,
      currentItem: this.props.data[0]
    };
  },
  render() {
    this._onSlide();
    return (
      <TouchableHighlight onPress={this._onPressButton}>
        <Image
          style={styles.items}
          source={{uri: this.state.currentItem.bannerPC}}/>
      </TouchableHighlight>
    );
  }
});

const styles = StyleSheet.create({
  items: {
    width: 250,
    height: 100
  },
  textTitle: {
    width: 250,
    height: 100
  }
});

module.exports = Slider;
