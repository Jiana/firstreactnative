'use strict';

import React, {
  Component,
  Text,
  View,
  Image,
  TouchableHighlight,
  NativeModules
} from 'react-native';

const styles = require('../style');

var MiddleAds = React.createClass({
  _onPressButton() {
  	NativeModules.HTMainViewController.hotelCd(this.props.htl.hotelId);
  },
  getInitialState() {
    return {
      imgSource: {uri: this.props.htl.photoURL},
      resizeMode: 'cover',
      imgHeight: 70,
      imgStyle: styles.imgView,
    }
  },
  imgStyle() {
    return ({
      height: this.state.imgHeight,
    })

  },
  render() {
    return (
	    <TouchableHighlight onPress={this._onPressButton}>
	    	<View>
          <View style={this.state.imgStyle}>
  		      <Image
  		        style={this.imgStyle()}
  		        resizeMode={this.state.resizeMode}
              onError={(e) => {
                this.setState({
                  imgSource: require('../imgs/picDefult.png'),
                  resizeMode: 'contain',
                  imgHeight: 42,
                  imgStyle: styles.imgViewDe4,
                })
              }}
  		        source={this.state.imgSource}/>
          </View>

          <View style={styles.floatTxt}>
  		      <Text style={styles.normalTxt}>
  		      	{this.props.htl.hotelName}
  		      </Text>
            <Text style={styles.priceTxt}>
              <Text style={styles.price}>
                {this.props.htl.salePriceAvg}
              </Text> 起
            </Text>
          </View>

	    	</View>
	    </TouchableHighlight>
    );
  }
});

module.exports = MiddleAds;
