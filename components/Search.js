'use strict';

import React, {
  Component,
  Text,
  View,
  Image,
  TextInput
} from 'react-native';

const styles = require('../style');

var Search = React.createClass({
  getInitialState() {
    return {
      inputTxt: null,
      dataFilted: this.props.data
    }
  },
  changeHandler(text) {
    this.state.dataFilted = this.props.data.filter((htl)=>{
      return htl.hotelName.indexOf(text) >= 0
    })
    this.props.onChange(this.state.dataFilted)
    this.forceUpdate()
  },
  render() {
    return (
      <View style={styles.searchBar}>
        <TextInput
          autoFocus={true}
          placeholder={"想去住..."}
          onChangeText={(text) => {
            console.log(text);
            this.state.inputTxt = text;
            this.forceUpdate();
            this.changeHandler(text);
          }}
          style={styles.searchInput}
          value={this.state.inputTxt}/>
      </View>
    );
  }
});

module.exports = Search;
